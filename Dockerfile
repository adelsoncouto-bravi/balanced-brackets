FROM openjdk:8-slim as builder

COPY BalancedBrackets.java /tmp/BalancedBrackets.java

RUN cd /tmp \
  ; javac BalancedBrackets.java

FROM openjdk:8-jre-slim

WORKDIR /usr/src

RUN mkdir -p /usr/src \
  ; cd /usr/src \
  ; echo '#!/bin/sh' > avaliar \
  ; echo 'java -Dfile.encoding=UTF8 BalancedBrackets "$1"' >> avaliar \
  ; chmod +x avaliar \
  ; ln -s /usr/src/avaliar /usr/sbin/avaliar

COPY --from=builder /tmp/BalancedBrackets.class /usr/src/BalancedBrackets.class
