# Balanced Brackets

Write a function that takes a string of brackets as the input, and determines if the order of the brackets is valid. A bracket is considered to be any one of the following characters: (, ), {, }, [, or ].
We say a sequence of brackets is valid if the following conditions are met:

* It contains no unmatched brackets.
* The subset of brackets enclosed within the confines of a matched pair of brackets is also a matched pair of brackets.

Examples:

* \(\)\{\}\[\] is valid
* \[\{\(\)\}\]\(\)\{\} is valid
* \[\]\{\(\) is not valid
* \[\{\)\] is not valid

# Sem docker

Compilar

```sh
javac BalancedBrackets.java
```

Uso

```sh
java -Dfile.encoding=UTF8 BalancedBrackets 'expressão para ser avaliada'
```

# Com docker

Gerar a build

```sh
docker build -t balanced-brackets:0.0.1 .
```

Executar

```sh
docker run --rm -it balanced-brackets:0.0.1 avaliar 'expressção para ser avaliada'
```



