import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BalancedBrackets {

	public static void main(String[] args) {
		
		//caso não seja informado nenhum parâmetros
		if(args.length == 0) {
			throw new RuntimeException("Você deve informar um expressão para ser avaliada");
		}
		
		//recupero a expressao
		String expressao = args[0];
		
		//retiro outros caracteres
		expressao = expressao.replaceAll("[^\\{\\(\\[\\]\\)\\}]+", "");
		
		//se ficou vazio, não tem nada a ser avaliado
		if(expressao.isEmpty()) {
			throw new RuntimeException("A expressão informada não contém {}, [], ou ()");
		}
		
		//regex para avaliar os pares
		Pattern pregex = Pattern.compile("(\\(\\)|\\{\\}|\\[\\])");
		Matcher mregex = pregex.matcher(expressao);
		
		//removo os pares se houver 
		while(mregex.find()) {
			expressao = mregex.replaceAll("");
			mregex = pregex.matcher(expressao);
		}
		//se ainda sobrar alguma coisa não é válida
		Boolean ok = expressao.isEmpty();
		
		if(ok) {
			System.out.println("A expressao \"".concat(args[0]).concat("\" é válida"));
		}else {
			System.err.println("A expressão \"".concat(args[0]).concat("\" não é válida"));
		}
		
	}

}
